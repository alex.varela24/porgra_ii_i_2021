package clase_1;

import java.util.Scanner;


public class Clase_1 {

    
    public static void main(String[] args) {
        
        Scanner var = new Scanner(System.in);
        int num1, num2;
        System.out.print("Digite el primer numero: ");
        num1 = var.nextInt();
        System.out.print("Digite el segundo numero: ");
        num2 = var.nextInt();
        
        if(num1 > num2){
           System.out.println("El mayor es: " + num1 + "El menor es: " + num2);
        }else{
            System.out.println("El mayor es: " + num2);
        }
        
        
        int[] arregloNumeros = new int [10];
        for (int i = 0; i < 10; i++){
            System.out.print("Digite el numero en la posicion " + (i) + ": ");
            arregloNumeros[i] = var.nextInt();
        }
        int suma=0;
        int promedio=0;
        int mayor=arregloNumeros[0];
        
        //Suma, mayor, promedio
        for(int i = 0; i < 10; i++){
            suma+=arregloNumeros[i];
            if(arregloNumeros[i] > mayor)
            {
                mayor=arregloNumeros[i];
            }
            System.out.println(String.format("Posicion [%d] Elemento: %d ", i, arregloNumeros[i]));
        }
        promedio=suma/arregloNumeros.length;
        System.out.println(String.format("Mayor: %d Suma: %d Promedio: %d ", mayor, suma, promedio));
    }
    
}
